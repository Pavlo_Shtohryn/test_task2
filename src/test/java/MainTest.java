import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainTest {
    WebDriver webDriver;
    WebElement webElement;
    static final String URL = "https://www.gmail.com/";
    static final String EMAIL1 = "tom.shtogryn.2004";
    static final String EMAIL2 = "pavlo.stogryn@gmail.com";
    static final String PASSWORD1 = "******";
    static String Message = "Hello";

    @BeforeEach
    public void initialSetting() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        webDriver = new ChromeDriver();
    }

    @Test
    public void gmailTest() {
        webDriver.get(URL);
        webElement = webDriver.findElement(By.cssSelector("input[type='email']"));
        webElement.sendKeys(EMAIL1);
        Assertions.assertNotNull(webDriver.getTitle());
        webDriver.findElement(By.xpath("//div[@class=\'dG5hZc\']//span")).click();
        (new WebDriverWait(webDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("input[type='password']")));
        webDriver.findElement(By.cssSelector("input[type='password']")).sendKeys(PASSWORD1);
        Assertions.assertNotNull(webDriver.getTitle());
        webDriver.findElement(By.xpath("//div[@id='passwordNext']")).click();
        (new WebDriverWait(webDriver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Compose']")));
        WebElement buttonCreateLetter = webDriver.findElement(By.xpath("//div[text()='Compose']"));
        buttonCreateLetter.click();
        webElement = webDriver.findElement(By.xpath("//form[@class='bAs']//textarea[@name='to']"));
        webElement.sendKeys(EMAIL2);
        webElement = webDriver.findElement(By.xpath("//form/div/input[@name='subjectbox']"));
        webElement.sendKeys(Message);
        webElement = webDriver.findElement(By.xpath("//div[@class='AD']//td/div/div[@role='textbox']"));
        webElement.sendKeys(Message);
        webElement = webDriver.findElement(By.xpath("//div[@class='AD']//tr[@class='btC']/td/div/div[@role='button']"));
        webElement.click();
    }

    @AfterEach
    public void quitDriver() {
        webDriver.quit();
    }
}
